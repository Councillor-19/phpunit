<?php

namespace TDD;

class Formatter
{
	public function currencyAmt($input): float
	{
		return round($input, 2);
	}
}
