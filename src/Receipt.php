<?php

namespace TDD;

use \BadMethodCallException;

class Receipt
{
	private $formatter;
	public $tax;

	public function __construct(Formatter $formatter)
	{
		$this->formatter = $formatter;
	}

	public function subtotal(array $items, float $coupon = null): float
	{
		if($coupon > 1.00) {
			throw new BadMethodCallException('Coupon must be less than or equal to 1.00');
		}
		$sum = array_sum($items);
		if(!is_null($coupon)) {
			return $sum - ($sum * $coupon);
		}
		return $sum;
	}

	public function tax(float $amount): float
	{
		$input = $amount * $this->tax;
		return $this->formatter->currencyAmt($input);
	}

	public function postTaxTotal(array $item, float $coupon = null): float
	{
		$subtotal = $this->subtotal($item, $coupon);
		return $subtotal + $this->tax($subtotal);
	}
}