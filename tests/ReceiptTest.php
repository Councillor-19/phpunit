<?php

namespace TDD\Test;

require dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

use PHPUnit\Framework\TestCase;
use TDD\Receipt;

class ReceiptTest extends TestCase
{
	public $receipt;
	private $formatter;

	public function setUp(): void
	{
		$this->formatter = $this->getMockBuilder('TDD\Formatter')
		                        ->setMethods(['currencyAmt'])
		                        ->getMock();
		$this->formatter->expects($this->any())
		                ->method('currencyAmt')
		                ->with($this->anything())
		                ->will($this->returnArgument(0));
		$this->receipt = new Receipt($this->formatter);
	}

	public function tearDown(): void
	{
		unset($this->receipt);
	}

	/**
	 * @dataProvider provideSubtotal
	 *
	 * @param $items
	 * @param $expected
	 */
	public function testSubtotal($items, $expected): void
	{
		$coupon = null;
		$output = $this->receipt->subtotal($items, $coupon);
		$this->assertEquals(
			$expected,
			$output,
			"When summing the total should equal {$expected}"
		);
	}

	public function provideSubtotal(): array
	{
		return [
			'inst totaling 16' => [[1,2,5,8], 16],
			[[-1,2,5,8], 14],
			[[1,2,8], 11]
		];
	}

	public function testSubtotalAndCoupon(): void
	{
		$input = [0,2,5,8];
		$coupon = 0.2;
		$output = $this->receipt->subtotal($input, $coupon);
		$this->assertEquals(
			12,
			$output,
			'When summing the total should equal 12'
		);
	}

	public function testSubTotalException(): void
	{
		$input = [0,2,5,8];
		$coupon = 1.20;
		$this->expectException('BadMethodCallException');
		$this->receipt->subtotal($input, $coupon);
	}

	public function testPostTaxTotal(): void
	{
		$receipt = $this->getMockBuilder('TDD\Receipt')
		                ->setMethods(['tax', 'subtotal'])
						->setConstructorArgs([$this->formatter])
		                ->getMock();

		$receipt->method('subtotal')
		        ->will($this->returnValue(10.00));
		$receipt->method('tax')
				->with(10.00)
		        ->will($this->returnValue(1.00));

		$result = $receipt->postTaxTotal([1,2,5,8], null);
		$this->assertEquals(11.00, $result);
	}

	public function testTax(): void
	{
		$inputAmount = 10.00;
		$this->receipt->tax = 0.10;
		$output = $this->receipt->tax($inputAmount);
		$this->assertEquals(
			1.00,
			$output,
			'The tax calculation should equal 1.00'
		);
	}
}
